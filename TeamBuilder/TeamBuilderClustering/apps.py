from django.apps import AppConfig


class TeambuilderclusteringConfig(AppConfig):
    name = 'TeamBuilderClustering'
