from django.core.validators import MinValueValidator
from django.db import models


class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)


class Student(models.Model):
    enumber = models.CharField(max_length=9, primary_key=True)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    days_busy = {'Monday': [], 'Tuesday': [], 'Wednesday': [], 'Thursday': [],
                 'Friday': [], 'Saturday': [], 'Sunday': []}

    def __str__(self):
        return self.first_name + " " + self.last_name


class Class(models.Model):
    class_name = models.CharField(max_length=30)
    students = models.ManyToManyField(Student)

    def __str__(self):
        return self.class_name
